// Convert an input .ass with N procedurals pointing to the same filename
// to an output .ass whit 1 procedural and N-1 ginstance to it.
// In theory this should reduce the init time.
// Usage: ProceduralsToGinstances <input ass> <output ass>

#define AI_ENABLE_DEPRECATION_WARNINGS 1

#include <ai.h>
#include <string>
#include <map>

using namespace std;

// strings I'll use:
const AtString s_filename      = AtString("filename");
const AtString s_inherit_xform = AtString("inherit_xform");
const AtString s_matrix        = AtString("matrix");
const AtString s_node          = AtString("node");
const AtString s_procedural    = AtString("procedural");
const AtString s_shader        = AtString("shader");
const AtString s_visibility    = AtString("visibility");

// Copy the more common user data from note src into node dest
//
void CopyUserData(AtNode* src, AtNode* dest)
{
    // iterate the user data
    AtUserParamIterator *it = AiNodeGetUserParamIterator(src);
    while (!AiUserParamIteratorFinished(it))
    {
        const AtUserParamEntry *param_entry = AiUserParamIteratorGetNext(it);
        AtString param_name(AiUserParamGetName(param_entry));
        uint8_t param_type = AiUserParamGetType(param_entry);
        string param_type_name(AiParamGetTypeName(param_type));
        uint8_t param_category = AiUserParamGetCategory(param_entry);
        // fprintf(stderr, "%s of type %s\n", param_name.c_str(), param_type_name.c_str());

        // available categories:
        //#define AI_USERDEF_UNDEFINED  0     /**< Undefined, you should never encounter a parameter of this category */
        //#define AI_USERDEF_CONSTANT   1     /**< User-defined: per-object parameter */
        //#define AI_USERDEF_UNIFORM    2     /**< User-defined: per-face parameter */
        //#define AI_USERDEF_VARYING    3     /**< User-defined: per-vertex parameter */
        //#define AI_USERDEF_INDEXED 

        string declarestr("");
        if (param_category == AI_USERDEF_CONSTANT)
            declarestr += "constant ";
        else if (param_category == AI_USERDEF_UNIFORM)
            declarestr += "uniform ";
        else
        {
            fprintf(stderr, "Lazy bastard, you are missing support for user data of categpry %d\n", param_category);
            continue;
        }

        switch (param_type)
        {
        case AI_TYPE_STRING:
        {
            declarestr += "STRING";
            AiNodeDeclare(dest, param_name, declarestr.c_str());
            AtString value = AiNodeGetStr(src, param_name);
            AiNodeSetStr(dest, param_name, value);
            break;
        }
        case AI_TYPE_INT:
        {
            declarestr += "INT";
            AiNodeDeclare(dest, param_name, declarestr.c_str());
            int value = AiNodeGetInt(src, param_name);
            AiNodeSetInt(dest, param_name, value);
            break;
        }
        case AI_TYPE_FLOAT:
        {
            declarestr += "AI_TYPE_FLOAT";
            AiNodeDeclare(dest, param_name, declarestr.c_str());
            float value = AiNodeGetFlt(src, param_name);
            AiNodeSetFlt(dest, param_name, value);
            break;
        }
        case AI_TYPE_RGB:
        {
            declarestr += "RGB";
            AiNodeDeclare(dest, param_name, declarestr.c_str());
            AtRGB value = AiNodeGetRGB(src, param_name);
            AiNodeSetRGB(dest, param_name, value.r, value.g, value.b);
            break;
        }
        case AI_TYPE_RGBA:
        {
            declarestr += "RGBA";
            AiNodeDeclare(dest, param_name, declarestr.c_str());
            AtRGBA value = AiNodeGetRGBA(src, param_name);
            AiNodeSetRGBA(dest, param_name, value.r, value.g, value.b, value.a);
            break;
        }
        case AI_TYPE_VECTOR:
        {
            declarestr += "VECTOR";
            AiNodeDeclare(dest, param_name, declarestr.c_str());
            AtVector value = AiNodeGetVec(src, param_name);
            AiNodeSetVec(dest, param_name, value.x, value.y, value.z);
            break;
        }
        default:
            fprintf(stderr, "Missing support for user data of type %s\n", param_type_name.c_str());
            continue;
            break;
        }
    }
    AiUserParamIteratorDestroy(it);
}

int main(int argc, char **argv)
{
    if (argc != 3)
    {
        fprintf(stderr, "Usage: ProceduralsToGinstances.exe <input ass> <output ass>\n");
        return 0;
    }

    // Map of the unique procedurals:
    // For each filename parameter of the procedurals (ex. \temp\stocazzo.ass), we store the first procedural using that filename
    // These masters will be preserved, and ginstanced by all the other procedurals that, in the input scene, point
    // to the same filename
    map <AtString, AtNode*> master_procedurals;

    AiMsgSetConsoleFlags(AI_LOG_ALL);
    AiBegin(AI_SESSION_BATCH);

    AiSceneLoad(nullptr, AtString(argv[1]), nullptr);

    int nb_procedurals = 0;
    // iterate the shape nodes
    AtNodeIterator* it = AiUniverseGetNodeIterator(nullptr, AI_NODE_SHAPE);
    while (!AiNodeIteratorFinished(it))
    {
        AtNode* node = AiNodeIteratorGetNext(it);
        // we just care fpr the procedurals
        if (!AiNodeIs(node, s_procedural))
            continue;

        nb_procedurals++;
        AtString filename = AiNodeGetStr(node, s_filename);
        // if filename is not in the master map yet, add it together with the procedural node
        if (master_procedurals.find(filename) == master_procedurals.end())
            master_procedurals[filename] = node;
    }

    fprintf(stderr, "nb_procedurals = %d\n", nb_procedurals);
    fprintf(stderr, "nb master filenames = %zd\n", master_procedurals.size());

    // iterate again
    it = AiUniverseGetNodeIterator(nullptr, AI_NODE_SHAPE);
    while (!AiNodeIteratorFinished(it))
    {
        AtNode* node = AiNodeIteratorGetNext(it);

        if (!AiNodeIs(node, s_procedural))
            continue;

        AtString filename = AiNodeGetStr(node, s_filename);

        // get the master procedural pointing to this filename
        AtNode* master_node = master_procedurals[filename];
        if (node == master_node)
        {
            // leave the master procedural in place in the universe
            continue; 
        }
        else
        {
            // replace the procedural with a ginstance node, pointing to the master procedural node

            // new node name = procedural name + "_ginstance". In case in the scene there are references to the
            // original procedural name (ex. using operators), the procedural's name should be used, so set on the ginstance
            // after having destroyed the procedural (names can not be shared between nodes). Let's assume this is not the case
            string procedural_name(AiNodeGetName(node));
            string ginstance_name = procedural_name + string("_ginstance");
            // create the ginstance
            AtNode* ginstance = AiNode(nullptr, AtString("ginstance"), AtString(ginstance_name.c_str()));
            // node to be instanced, that is the master procedural
            AiNodeSetPtr(ginstance, s_node, master_node);

            // copy the matrix array, if any
            AtArray* matrix = AiNodeGetArray(node, s_matrix);
            if (matrix != nullptr)
                AiNodeSetArray(ginstance, s_matrix, AiArrayCopy(matrix));

            // copy the visibility mask
            uint8_t visibility = AiNodeGetByte(node, s_visibility);
            AiNodeSetByte(ginstance, s_visibility, visibility);

            // ginstances should not inherint the master's transformation:
            AiNodeSetBool(ginstance, s_inherit_xform, false);

            // copy all the user data
            CopyUserData(node, ginstance);

            // destroy the procedural
            AiNodeDestroy(node);
        }
    }

    // destroy the iterator
    AiNodeIteratorDestroy(it);
    // write the modified universe into the output .ass file
    // params for writing out:
    AtParamValueMap* params = AiParamValueMap();
    // int nodeMask = AI_NODE_CAMERA | AI_NODE_LIGHT | AI_NODE_SHAPE | AI_NODE_SHADER | AI_NODE_OVERRIDE | AI_NODE_COLOR_MANAGER | AI_NODE_OPERATOR;
    int nodeMask = AI_NODE_SHAPE;
    AiParamValueMapSetInt(params, AtString("mask"), nodeMask);
    AiParamValueMapSetBool(params, AtString("binary"), true);

    AiSceneWrite(nullptr, AtString(argv[2]), nullptr, nullptr);
    AiParamValueMapDestroy(params);
    // done
    AiEnd();
    return 0;
}
















